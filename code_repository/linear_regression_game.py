"""
Tools to compute equilibria of the linear regression game
"""
import scipy.optimize as opt
import numpy as np


def var(strategies, X, probabilities, number_agents):
    """Returns the covariance matrix in the game given:
    - The strategies of the players
    - The set of possible vectors X
    - The probability of presence of each vector
    - The non strategic points
    - The non strategic precisions"""
    d = len(X[0])  # dimension
    m = len(X)  # number of different vectors
    n = int(len(strategies) / m)  # number of agents
    avg_var = np.zeros((d, d))
    for player in range(n):
        for vector in range(m):
            avg_var += number_agents[player] * probabilities[vector] * strategies[player *
                        m + vector] * np.tensordot(X[vector], X[vector], axes=0)
    return np.matrix(avg_var)**(-1)


def potential_mean(strategies, X, probabilities, scalarization, costs, args, number_agents):
    """The value of the potential function in the approximate covariance game given:
    - The strategies of the players
    - The set of possible data points X
    - The probability distribution of points in the set of data points
    - The exponent of each players monomial privacy cost
    - The non strategic points and precisions
    - The scalarization considered
    """
    indiv_cost = 0
    n = len(costs)
    m = len(X)
    for player in range(n):
        for vector in range(m):
            indiv_cost += number_agents[player] * probabilities[vector] * (
                costs[player](strategies[player * m + vector], args[player])
            )
    avg_var = var(strategies, X, probabilities, number_agents)
    mean_estim_cost = scalarization(avg_var, X, probabilities)
    return indiv_cost + mean_estim_cost


def covariance_equilibrium(X, probabilities, scalarization, costs, args, number_agents=None):
    """Returns the equilibrium of the game given:
    - The set of possible data points X
    - The probability distribution of data points
    - The exponent of each player monomial privacy cost
    - The non strategic points and precisions
    - The scalarization considered"""
    starting_point = 1
    n = len(costs)  # Number of players
    if(number_agents is None):
        number_agents = [1 for i in range(n)]
    m = len(X)  # Number of vectors
    strategies0 = [[starting_point for j in range(n * m)]]
    bnds = [[1e-10, np.inf] for i in range(n * m)]
    tol = 1e-20
    sol = opt.minimize(potential_mean, strategies0, args=(
        X, probabilities, scalarization, costs, args, number_agents),
        bounds=bnds, tol=tol)
    return sol.x


def design_value(design, X, probabilities, scalarization):
    d = len(X[0])
    var = np.zeros((d, d))
    for i, vector in enumerate(X):
        var += design[i] * np.tensordot(vector, vector, axes=0)
    return scalarization(np.matrix(var)**(-1), X, probabilities)


def optimal_design(X, probabilities, scalarization):
    initial_design = [1 / len(X) for x in X]
    bnds = [[0, 1] for x in X]
    tol = 1e-10
    cons = ({'type': 'eq', 'fun': lambda x: np.array(sum(x) - 1)})
    sol = opt.minimize(design_value, initial_design, constraints=cons, args=(
        X, probabilities, scalarization), bounds=bnds, tol=tol)
    return sol.x
