"""
Generic functions (costs) shared by all games
"""
import numpy as np
import math


def monomial_cost(l, exponent):
    """ Returns the cost a agent endures for producing data with precision l
        in case of monomial cost function"""
    return math.pow(l, exponent)


def polynomial_cost(l, coefs):
    polynom = 0
    for i, coef in enumerate(coefs):
        polynom += coef * (l ** i)
    return polynom


def cosh(l, arg):
    return math.cosh(l) - 1


def trace_scalarization(variance, X, probabilities):
    """ Returns the estimation cost associated to the trace scalarization
    given the covariance matrix and the set of possible vectors"""
    return np.trace(variance)


def trace_wihout1stcoordinate(variance, X, probabilities):
    """ Returns the estimation cost associated to the trace scalarization
    given the covariance matrix and the set of possible vectors"""
    return np.trace(variance) - variance[0, 0]


def mean_square_error(variance, X, probabilities):
    """ Returns the MSE (sum of E[(y_i^tilde - y_i)^2])
    given the covariance matrix and the set of possible vectors"""
    error = 0
    for i in range(len(X)):
        x = X[i]
        array_var = np.asarray(variance)
        vector = np.asarray(x)
        error += np.dot(vector.T, np.dot(array_var, vector)) * probabilities[i]
    return error


def square_frobenius(variance, X, probabilities):
    return np.linalg.norm(variance, ord='fro')**2


def mean_square_error_without1st(variance, X, probabilities):
    """ Returns the MSE (sum of E[(y_i^tilde - y_i)^2])
    given the covariance matrix and the set of possible vectors"""
    error = 0
    for i in range(len(X)):
        x = X[i]
        vector = np.asmatrix(x[1:])
        var = np.asmatrix(variance[1:, 1:])
        error += np.dot(vector, np.dot(var, vector.T)) * probabilities[i]
    return error
