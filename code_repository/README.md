# Asymptotic degradation of linear regression with strategic data sources and a public good component

This repository is the official implementation of the paper "Asymptotic degradation of linear regression estimates with strategic data sources".

## Requirements

All librairies used in this repository are common python libraries (`scipy`, `numpy` and `matplotlib`). They can be installed by:

```setup
pip install -r requirements.txt
```

## Illustrations

This repository only contains the code to generate all the illustrations present in the paper and in the supplementary material. To generate them, just run the notebooks `figures_paper.ipynb` and  `exploration_theorem_3.ipynb` . The notebooks indentify which part of the code to run to generate each of the figure of the paper.
